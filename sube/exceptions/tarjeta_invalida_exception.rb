class TarjetaInvalida < Exception
  def initialize(msg = 'La tarjeta que busca no existe en el sistema.')
    super
  end
end