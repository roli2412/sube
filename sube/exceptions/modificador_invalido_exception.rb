class ModificadorDePrecioInvalido < Exception
  def initialize(msg = 'El modificador introducido es invalido.')
    super
  end
end
