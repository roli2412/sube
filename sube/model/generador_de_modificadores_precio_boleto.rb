require_relative '../model/descuento_jubilado'
require_relative '../model/descuento_docente'
require_relative '../model/descuento_estudiante'
require_relative '../model/recargo_extranjero'
require_relative '../exceptions/modificador_invalido_exception'

class GeneradorModificadoresDePrecioBoleto
  attr_accessor :modificador
  MODIFICADORES = {'jubilado' => DescuentoJubilado, 'docente' => DescuentoDocente, 'estudiante' => DescuentoEstudiante, 'extrajero' => RecargoExtranjero, 'extranjero' => RecargoExtranjero}

  def validar_modificador(tipo_modificador)
  	if !MODIFICADORES.key?(tipo_modificador)
  	  raise ModificadorDePrecioInvalido
  	end 
  end

  def generar_modificador(tipo_modificador)
    @modificador = tipo_modificador
    if tipo_modificador != "bloqueado"
      self.validar_modificador(tipo_modificador)
      @modificador = MODIFICADORES[tipo_modificador].new
    end
  end

  def get_modificador_generado
  	@modificador
  end
end