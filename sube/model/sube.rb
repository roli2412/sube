require_relative './descuento_dos_horas'

class Sube
  attr_accessor :saldo, :usuario, :dni, :id, :boletos, :modificadores, :bloqueada, :tipo_modificadores

  def initialize(id)
    @id = id
    @boletos = []
    @modificadores = []
    @bloqueada = false
    @saldo = 0
    @tipo_modificadores = []
  end

  def agregar_modificador_precio_boleto(modificador)
    if !modificador.nil?
      @modificadores.push(modificador)    
    end
  end

  def agregar_tipo_modificador_precio_boleto(tipo_modificador)
    if tipo_modificador == "bloqueado"
      self.bloquear_tarjeta
    end
    @tipo_modificadores.push(tipo_modificador)
  end

  def get_tipo_modificadores_precio_boleto
    @tipo_modificadores
  end  

  def cargar_saldo(saldo_ingresado)
  	@saldo = @saldo + saldo_ingresado
  end

  def get_saldo
  	@saldo
  end

  def get_id
    @id
  end

  def get_boletos
    @boletos
  end

  def realizar_pago(boleto)
    monto_final_boleto = 0
    if !@bloqueada
      @boletos.push(boleto)
      modificador_de_precio = self.aplica_moficiador_de_precio_boleto(boleto)
      monto_final_boleto = boleto.get_monto - modificador_de_precio 
      if @saldo >  monto_final_boleto
        @saldo = @saldo - monto_final_boleto      
      else
        monto_final_boleto = 0
      end      
    end
    monto_final_boleto
  end

  def aplica_moficiador_de_precio_boleto(boleto)
    descuento_segundo_boleto = DescuentoDosHoras.new
    modificacion_precio_total = descuento_segundo_boleto.aplicar_modificacion_precio(@boletos)
    if @modificadores.length != 0
      @modificadores.each do |modificador|
        modificacion_precio_total = modificacion_precio_total + modificador.aplicar_modificacion_precio(boleto)
      end      
    end
    modificacion_precio_total
  end

  def bloquear_tarjeta
    @bloqueada = true
  end

  def desbloquear_tarjeta
    @bloqueada = false    
  end
end 