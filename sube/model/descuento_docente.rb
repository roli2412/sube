class DescuentoDocente
  DESCUENTO = 0.3
  MESES_DESCUENTO = ['MAR','APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']

  def aplicar_modificacion_precio(boleto)
  	descuento_otorgado = 0
  	MESES_DESCUENTO.each do |mes|
	  if boleto.get_fecha.asctime.upcase.include?(mes)
	  	descuento_otorgado = boleto.get_monto * DESCUENTO  
	  end
	end
	descuento_otorgado
  end
end