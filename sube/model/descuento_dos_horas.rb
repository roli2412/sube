class DescuentoDosHoras
  DESCUENTO = 0.5

  def aplicar_modificacion_precio(boletos)
    descuento_otorgado = 0
  	cantidad_boletos = boletos.length
  	ultimo_boleto = boletos[cantidad_boletos - 1] 
  	if cantidad_boletos >= 2
  	  if(ultimo_boleto.get_fecha - boletos[cantidad_boletos - 2].get_fecha) <= 7200
        descuento_otorgado = ultimo_boleto.get_monto * DESCUENTO  
  	  end	
  	end
    descuento_otorgado
  end
end