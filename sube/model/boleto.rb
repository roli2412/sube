class Boleto
  attr_accessor :monto, :fecha

  def initialize(monto, fecha)
  	@fecha = fecha
  	@monto = monto
  end

  def get_fecha
  	@fecha
  end

  def get_monto
  	@monto
  end

  def set_monto(monto)
  	@monto = monto
  end
end