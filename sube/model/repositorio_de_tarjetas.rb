class RepositorioDeTarjetas
  attr_accessor :tarjetas

  def initialize
  	@tarjetas = []  		
    @salida_tarjetas = []
  end

  def agregar_tarjeta(tarjeta_nueva)
    @tarjetas.each do |tarjeta|
  	  if tarjeta.get_id == tarjeta_nueva.get_id
  	    @tarjetas.delete(tarjeta)	
  	  end	
  	end
  	@tarjetas.push(tarjeta_nueva)
  end

  def buscar_tarjeta(id)
    tarjeta_buscada = nil
    @tarjetas.each do |tarjeta|
      if tarjeta.get_id == id
        tarjeta_buscada = tarjeta
      end		
    end
    tarjeta_buscada
  end

  def get_tarjetas_salida
    @tarjetas.each do |tarjeta|
      id = tarjeta.get_id
      saldo = tarjeta.get_saldo.to_s
      salida = '{"id":' + id + ', "saldo":' + saldo + '}' 
      @salida_tarjetas.push(salida)
    end
    @salida_tarjetas[0] = '[' + @salida_tarjetas[0]
    @salida_tarjetas[@salida_tarjetas.length - 1] = @salida_tarjetas[@salida_tarjetas.length - 1] + ']'
    @salida_tarjetas.to_s
  end
end 