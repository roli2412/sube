class DescuentoEstudiante
  DESCUENTO = 0.7
  DIAS_DESCUENTO = ['MON','TUE', 'WES', 'THU', 'FRI']

  def aplicar_modificacion_precio(boleto)
	descuento_otorgado = 0    	
  	DIAS_DESCUENTO.each do |dia|
	  if boleto.get_fecha.asctime.upcase.include?(dia)
	  	descuento_otorgado = boleto.get_monto * DESCUENTO  
	  end
	end
	descuento_otorgado  
  end
end