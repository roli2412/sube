require_relative '../model/boleto'

describe 'Boleto' do   
  it 'El monto a pagar deberia ser de $10' do
  	fecha = Time.new(2018, 12, 16, 20, 0, 0)
  	monto = 10
  	boleto = Boleto.new(monto, fecha)
  	valor_esperado = monto

  	valor_obtenido = boleto.get_monto
    
    expect(valor_obtenido).to eq valor_esperado
  end
 
  it 'La hora del boleto deberia ser la actual' do
  	fecha = Time.new(2018, 12, 16, 20, 0, 0)
  	boleto = Boleto.new(15, fecha)
  	valor_esperado = fecha

  	valor_obtenido = boleto.get_fecha
    
    expect(valor_obtenido.to_s).to eq valor_esperado.to_s
  end  
end