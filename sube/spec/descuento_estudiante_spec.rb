require_relative '../model/descuento_estudiante'
require_relative '../model/boleto'


describe 'Descuento estudiante' do   
  it 'Deberia aplicar descuento de $7 a boleto de $10 cuando se pago un dia lunes' do
  	fecha = Time.new(2018, 12, 17, 20, 0, 0)
  	boleto = Boleto.new(10, fecha)
  	descuento = DescuentoEstudiante.new

  	valor_esperado = 7

  	valor_obtenido = descuento.aplicar_modificacion_precio(boleto)
    
    expect(valor_obtenido).to eq valor_esperado
  end

  it 'No deberia aplicar descuento cuando se pago un boleto un dia sabado' do
    fecha = Time.new(2018, 12, 15, 20, 0, 0)
    boleto = Boleto.new(10, fecha)
    descuento = DescuentoEstudiante.new

    valor_esperado = 0

    valor_obtenido = descuento.aplicar_modificacion_precio(boleto)
    
    expect(valor_obtenido).to eq valor_esperado
  end
end