require_relative '../model/descuento_docente'
require_relative '../model/boleto'

describe 'Descuento docente' do   
  it 'Deberia aplicar descuento de $3 a boleto de $10 cuando se pago en el mes de Junio' do
    fecha = Time.new(2018, 6, 17, 20, 0, 0)
    boleto = Boleto.new(10, fecha)
    descuento = DescuentoDocente.new

    valor_esperado = 3

    valor_obtenido = descuento.aplicar_modificacion_precio(boleto)
    
    expect(valor_obtenido).to eq valor_esperado
  end

  it 'No deberia aplicar descuento cuando se pago un boleto en el mes de Enero' do
    fecha = Time.new(2018, 1, 17, 20, 0, 0)
    boleto = Boleto.new(10, fecha)
    descuento = DescuentoDocente.new

    valor_esperado = 0 

    valor_obtenido = descuento.aplicar_modificacion_precio(boleto)
    
    expect(valor_obtenido).to eq valor_esperado
  end
end