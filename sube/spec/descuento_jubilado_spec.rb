require_relative '../model/descuento_jubilado'
require_relative '../model/boleto'

describe 'Descuento jubilado' do   
  it 'Deberia aplicar descuento de $4 a boleto de $10' do
  	fecha = Time.new(2018, 12, 17, 20, 0, 0)
  	boleto = Boleto.new(10, fecha)
  	descuento = DescuentoJubilado.new

  	valor_esperado = 4

  	valor_obtenido = descuento.aplicar_modificacion_precio(boleto)
    
    expect(valor_obtenido).to eq valor_esperado
  end
end