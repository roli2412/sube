require_relative '../model/generador_de_modificadores_precio_boleto'
require_relative '../exceptions/modificador_invalido_exception'

describe 'Generador de Modificadores de Precio' do   
  it 'Deberia generar un DescuentoJubilado' do
    generador_descuento = GeneradorModificadoresDePrecioBoleto.new
    valor_esperado = DescuentoJubilado

    valor_obtenido = generador_descuento.generar_modificador('jubilado')

    expect(valor_obtenido).to be_a(valor_esperado)
  end

  it 'Deberia generar un DescuentoDocente' do
    generador_descuento = GeneradorModificadoresDePrecioBoleto.new
    valor_esperado = DescuentoDocente

    valor_obtenido = generador_descuento.generar_modificador('docente')

    expect(valor_obtenido).to be_a(valor_esperado)
  end 

  it 'Deberia generar un DescuentoEstudiante' do
    generador_descuento = GeneradorModificadoresDePrecioBoleto.new
    valor_esperado = DescuentoEstudiante

    valor_obtenido = generador_descuento.generar_modificador('estudiante')

    expect(valor_obtenido).to be_a(valor_esperado)
  end 

  it 'Deberia devolver excepcion ModificadorDePrecioInvalido cuando se introduce tipo de modificador "mecanico"' do
    generador_descuento = GeneradorModificadoresDePrecioBoleto.new

    expect{generador_descuento.generar_modificador('mecanico')}.to raise_error(ModificadorDePrecioInvalido)
  end 
end