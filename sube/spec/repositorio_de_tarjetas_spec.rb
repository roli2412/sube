require_relative '../model/sube'
require_relative '../model/repositorio_de_tarjetas'
require_relative '../exceptions/tarjeta_invalida_exception'

describe 'Repositorio de Tarjetas' do   
  it 'Deberia encontrar una tarjeta con id "rpasquali"' do
  	sube = Sube.new('rpasquali')
  	repositorio_de_tarjetas = RepositorioDeTarjetas.new
  	repositorio_de_tarjetas.agregar_tarjeta(sube)
  	valor_esperado = sube

  	valor_obtenido = repositorio_de_tarjetas.buscar_tarjeta('rpasquali')
    
    expect(valor_obtenido).to eq valor_esperado
  end

  it 'Deberia lanzar excepcion TarjetaInvalida cuando se busca con el id "dmartinez"' do
  	sube1 = Sube.new('rpasquali')
  	sube2 = Sube.new('jose1')
  	sube3 = Sube.new('pepe3')

  	repositorio_de_tarjetas = RepositorioDeTarjetas.new
  	repositorio_de_tarjetas.agregar_tarjeta(sube1)
  	repositorio_de_tarjetas.agregar_tarjeta(sube2)
  	repositorio_de_tarjetas.agregar_tarjeta(sube3)
    
    expect{repositorio_de_tarjetas.buscar_tarjeta('dmartinez')}.to raise_error(TarjetaInvalida)
  end

  it 'Deberia sobre escribir informacion de tarjeta si se registra una nueva con mismo id' do
  	sube1 = Sube.new('rpasquali')
  	sube1.cargar_saldo(15)
  	repositorio_de_tarjetas = RepositorioDeTarjetas.new
  	repositorio_de_tarjetas.agregar_tarjeta(sube1)
  	sube2 = Sube.new('rpasquali')
  	sube2.cargar_saldo(99)
  	repositorio_de_tarjetas.agregar_tarjeta(sube2)

  	valor_esperado = 99

  	valor_obtenido = repositorio_de_tarjetas.buscar_tarjeta('rpasquali').get_saldo
    
    expect(valor_obtenido).to eq valor_esperado
  end
end