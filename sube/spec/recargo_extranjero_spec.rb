require_relative '../model/recargo_extranjero'
require_relative '../model/boleto'

describe 'Recargo extranjero' do   
  it 'Deberia aplicar un recargo de $2 a un boleto de $10' do
  	fecha = Time.new(2018, 12, 17, 20, 0, 0)
  	boleto = Boleto.new(10, fecha)
  	recargo = RecargoExtranjero.new

  	valor_esperado = -2
  	
  	valor_obtenido = recargo.aplicar_modificacion_precio(boleto)
    
    expect(valor_obtenido).to eq valor_esperado
  end
end