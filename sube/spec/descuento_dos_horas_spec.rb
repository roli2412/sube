require_relative '../model/descuento_dos_horas'
require_relative '../model/boleto'

describe 'Descuento 2 horas' do   
  it 'Deberia aplicar descuento de $10 para un boleto de $20, cuando hay 2 boletos' do
  	fecha1 = Time.new(2018, 12, 16, 20, 0, 0)
  	fecha2 = Time.new(2018, 12, 16, 21, 0, 0)
  	boleto1 = Boleto.new(15, fecha1)
  	boleto2 = Boleto.new(20, fecha2)
  	boletos = [boleto1, boleto2]
  	descuento = DescuentoDosHoras.new

  	valor_esperado = 10

  	valor_obtenido = descuento.aplicar_modificacion_precio(boletos)
    
    expect(valor_obtenido).to eq valor_esperado
  end

  it 'Deberia aplicar descuento de $15 para un boleto de $30, cuando hay 4 boletos' do
  	fecha1 = Time.new(2018, 12, 15, 1, 0, 0)
  	fecha2 = Time.new(2018, 12, 10, 5, 0, 0)
  	fecha3 = Time.new(2018, 12, 16, 20, 0, 0)
  	fecha4 = Time.new(2018, 12, 16, 22, 0, 0)
  	boleto1 = Boleto.new(15, fecha1)
  	boleto2 = Boleto.new(20, fecha2)
  	boleto3 = Boleto.new(25, fecha3)
  	boleto4 = Boleto.new(30, fecha4)
  	boletos = [boleto1, boleto2, boleto3, boleto4]
  	descuento = DescuentoDosHoras.new

  	valor_esperado = 15

  	valor_obtenido = descuento.aplicar_modificacion_precio(boletos)
    
    expect(valor_obtenido).to eq valor_esperado
  end

  it 'No deberia aplicar descuento cuando hay 1 boleto' do
  	fecha = Time.new(2018, 12, 16, 20, 0, 0)
  	boleto = Boleto.new(15, fecha)
  	boletos = [boleto]
  	descuento = DescuentoDosHoras.new

  	valor_esperado = 0
  	
  	valor_obtenido = descuento.aplicar_modificacion_precio(boletos)
    
    expect(valor_obtenido).to eq valor_esperado
  end
end