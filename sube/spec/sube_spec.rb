require_relative '../model/sube'
require_relative '../model/boleto'
require_relative '../model/generador_de_modificadores_precio_boleto'

describe 'Sube' do   
  it 'Deberia poder carga $20 de saldo en la tarjeta' do
  	sube = Sube.new('RodrigoP')
  	sube.cargar_saldo(20)
  	valor_esperado = 20

  	valor_obtenido = sube.get_saldo
    
    expect(valor_obtenido).to eq valor_esperado
  end

  it 'El id deberia ser JErnesto' do
  	sube = Sube.new('JErnesto')
  	valor_esperado = 'JErnesto'

  	valor_obtenido = sube.get_id

    expect(valor_obtenido).to eq valor_esperado
  end

  it 'Deberia poser $95 de saldo cuando se paga boleto de $5 y se tenia un saldo de $100' do
    sube = Sube.new('JErnesto')
    sube.cargar_saldo(100)
    monto_boleto = 5
    fecha_boleto = Time.new(2018, 12, 16, 20, 0, 0)    
    boleto = Boleto.new(monto_boleto, fecha_boleto)
    valor_esperado = 95 
    sube.realizar_pago(boleto)
    valor_obtenido = sube.get_saldo

    expect(valor_obtenido).to eq valor_esperado  
  end

  it 'Deberia poser 3 boletos pagos' do
    sube = Sube.new('JErnesto')
    sube.cargar_saldo(100)
    fecha_boleto1 = Time.new(2018, 12, 16, 20, 0, 0)    
    boleto1 = Boleto.new(5, fecha_boleto1)
    fecha_boleto2 = Time.new(2018, 12, 18, 20, 0, 0)    
    boleto2 = Boleto.new(5, fecha_boleto2)
    fecha_boleto3 = Time.new(2018, 12, 17, 20, 0, 0)    
    boleto3 = Boleto.new(5, fecha_boleto3)
    valor_esperado = [boleto1, boleto2, boleto3]

    sube.realizar_pago(boleto1)
    sube.realizar_pago(boleto2)
    sube.realizar_pago(boleto3)

    valor_obtenido = sube.get_boletos

    expect(valor_obtenido).to eq valor_esperado  
  end

  it 'Deberia devolver como saldo $55 cuando se pagan 1 boleto con descuento por las 2 horas y 2 sin descuentos' do
    sube = Sube.new('Ernesto')
    sube.cargar_saldo(100)
    fecha_boleto1 = Time.new(2018, 12, 16, 20, 0, 0)    
    boleto1 = Boleto.new(10, fecha_boleto1)
    fecha_boleto2 = Time.new(2018, 12, 17, 20, 0, 0)    
    boleto2 = Boleto.new(20, fecha_boleto2)
    fecha_boleto3 = Time.new(2018, 12, 17, 20, 38, 0)    
    boleto3 = Boleto.new(30, fecha_boleto3)
    valor_esperado = 55

    sube.realizar_pago(boleto1)
    sube.realizar_pago(boleto2)
    sube.realizar_pago(boleto3)

    valor_obtenido = sube.get_saldo

    expect(valor_obtenido).to eq valor_esperado  
  end

  it 'Deberia devolver como saldo $40 cuando se pagan 3 boletos sin descuentos' do
    sube = Sube.new('Ernesto')
    sube.cargar_saldo(100)
    fecha_boleto1 = Time.new(2018, 12, 16, 20, 0, 0)    
    boleto1 = Boleto.new(10, fecha_boleto1)
    fecha_boleto2 = Time.new(2018, 12, 17, 20, 0, 0)    
    boleto2 = Boleto.new(20, fecha_boleto2)
    fecha_boleto3 = Time.new(2018, 12, 18, 20, 38, 0)    
    boleto3 = Boleto.new(30, fecha_boleto3)
    valor_esperado = 40

    sube.realizar_pago(boleto1)
    sube.realizar_pago(boleto2)
    sube.realizar_pago(boleto3)

    valor_obtenido = sube.get_saldo

    expect(valor_obtenido).to eq valor_esperado  
  end

  it 'Deberia poser $97 de saldo cuando se paga boleto de $10, posee descuento estudiante y se tenia un saldo de $100' do
    sube = Sube.new('Ernesto')
    sube.cargar_saldo(100)
    monto_boleto = 10
    fecha_boleto = Time.new(2018, 10, 16, 20, 0, 0)    
    boleto = Boleto.new(monto_boleto, fecha_boleto)
    generador_descuento = GeneradorModificadoresDePrecioBoleto.new
    
    descuento_estudiante = generador_descuento.generar_modificador('estudiante')

    sube.agregar_modificador_precio_boleto(descuento_estudiante)
    valor_esperado = 97

    sube.realizar_pago(boleto)
    valor_obtenido = sube.get_saldo

    expect(valor_obtenido).to eq valor_esperado  
  end

  it 'Deberia poser $93 de saldo cuando se paga boleto de $10, posee descuento docente y se tenia un saldo de $100' do
    sube = Sube.new('Ernesto')
    sube.cargar_saldo(100)
    monto_boleto = 10
    fecha_boleto = Time.new(2018, 10, 16, 20, 0, 0)    
    boleto = Boleto.new(monto_boleto, fecha_boleto)
    generador_descuento = GeneradorModificadoresDePrecioBoleto.new
    descuento_docente = generador_descuento.generar_modificador('docente')

    sube.agregar_modificador_precio_boleto(descuento_docente)
    valor_esperado = 93

    sube.realizar_pago(boleto)
    valor_obtenido = sube.get_saldo

    expect(valor_obtenido).to eq valor_esperado  
  end 

  it 'Cuando el saldo es menor al pago del boleto, no se paga el boleto y el saldo no se modifica' do
    sube = Sube.new('Ernesto')
    sube.cargar_saldo(5)
    monto_boleto = 10
    fecha_boleto = Time.new(2018, 10, 16, 20, 0, 0)    
    boleto = Boleto.new(monto_boleto, fecha_boleto)
    valor_esperado = 5

    sube.realizar_pago(boleto)
    valor_obtenido = sube.get_saldo

    expect(valor_obtenido).to eq valor_esperado  
  end 

  it 'Deberia poser $88 de saldo cuando se paga boleto de $10, posee recargo extranjero y se tenia un saldo de $100' do
    sube = Sube.new('Ernesto')
    sube.cargar_saldo(100)
    monto_boleto = 10
    fecha_boleto = Time.new(2018, 10, 16, 20, 0, 0)    
    boleto = Boleto.new(monto_boleto, fecha_boleto)
    valor_esperado = 88
    generador_recargo = GeneradorModificadoresDePrecioBoleto.new
    recargo_extranjero = generador_recargo.generar_modificador('extranjero')
    sube.agregar_modificador_precio_boleto(recargo_extranjero)

    sube.realizar_pago(boleto)
    valor_obtenido = sube.get_saldo

    expect(valor_obtenido).to eq valor_esperado  
  end 

  it 'Deberia poser $91 de saldo cuando se paga boleto de $10, posee recargo extranjero, descuento docente y se tenia un saldo de $100' do
    sube = Sube.new('Ernesto')
    sube.cargar_saldo(100)
    monto_boleto = 10
    fecha_boleto = Time.new(2018, 10, 16, 20, 0, 0)    
    boleto = Boleto.new(monto_boleto, fecha_boleto)
    valor_esperado = 91
    generador_recargo = GeneradorModificadoresDePrecioBoleto.new
    recargo_extranjero = generador_recargo.generar_modificador('extranjero')
    sube.agregar_modificador_precio_boleto(recargo_extranjero)
    descuento_docente = generador_recargo.generar_modificador('docente')
    sube.agregar_modificador_precio_boleto(descuento_docente)

    sube.realizar_pago(boleto)
    valor_obtenido = sube.get_saldo

    expect(valor_obtenido).to eq valor_esperado  
  end

  it 'Deberia poser $95 de saldo cuando se paga boleto de $5, tarjeta no esta bloqueada y se tenia un saldo de $100' do
    sube = Sube.new('JErnesto')
    sube.cargar_saldo(100)
    monto_boleto = 5
    fecha_boleto = Time.new(2018, 12, 16, 20, 0, 0)    
    boleto = Boleto.new(monto_boleto, fecha_boleto)
    valor_esperado = 95 
    sube.realizar_pago(boleto)
    valor_obtenido = sube.get_saldo

    expect(valor_obtenido).to eq valor_esperado  
  end

  it 'No deberia poder pagar boleto cuando la tarjeta esta bloqueada' do
    sube = Sube.new('JErnesto')
    sube.cargar_saldo(100)
    sube.bloquear_tarjeta
    monto_boleto = 5
    fecha_boleto = Time.new(2018, 12, 16, 20, 0, 0)    
    boleto = Boleto.new(monto_boleto, fecha_boleto)
    valor_esperado = 100 
    sube.realizar_pago(boleto)
    valor_obtenido = sube.get_saldo

    expect(valor_obtenido).to eq valor_esperado  
  end  
end