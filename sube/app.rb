require 'sinatra'
require 'sinatra/json'
require 'json'
require_relative './model/sube'
require_relative './model/repositorio_de_tarjetas'
require_relative './model/boleto'
require_relative './model/generador_de_modificadores_precio_boleto'

REPOSITORIO = RepositorioDeTarjetas.new
GENERADOR_DE_MODIFICADORES = GeneradorModificadoresDePrecioBoleto.new

post '/tarjetas' do
  params = JSON.parse(request.body.read)
  id = params['id']
  monto = params['monto'].to_i
  monto_a_cargar = params['monto_a_cargar'].to_i
  if monto_a_cargar > 0 
		tarjeta = REPOSITORIO.buscar_tarjeta(id)
  	tarjeta.cargar_saldo(monto_a_cargar)
  else
		tarjeta = Sube.new(id)
  	tarjeta.cargar_saldo(monto)
  end

  REPOSITORIO.agregar_tarjeta(tarjeta)

  halt 201, json({"saldo":tarjeta.get_saldo.to_s,"id":tarjeta.get_id})  	
end

put '/tarjetas' do
  params = JSON.parse(request.body.read)
  id = params['id']
  tipo_modificadores = params['modificador'].split(',')
  monto_a_cargar = params['monto_a_cargar'].to_i
	tarjeta = REPOSITORIO.buscar_tarjeta(id)
  if monto_a_cargar != ""
	  tarjeta.cargar_saldo(monto_a_cargar)
	end
	if tipo_modificadores != []
		tipo_modificadores.each do |tipo_modificador|
			  modificador = GENERADOR_DE_MODIFICADORES.generar_modificador(tipo_modificador)  	
			  tarjeta.agregar_modificador_precio_boleto(modificador)
			  tarjeta.agregar_tipo_modificador_precio_boleto(tipo_modificador)
		end
	end
  REPOSITORIO.agregar_tarjeta(tarjeta)

	halt 200, json({"saldo":tarjeta.get_saldo.to_i,"id":tarjeta.get_id})
end

post '/pagos' do
  params = JSON.parse(request.body.read)
  id = params['id']
  monto_boleto = params['monto'].to_i
  boleto = Boleto.new(monto_boleto, Time.now)
	tarjeta = REPOSITORIO.buscar_tarjeta(id)
 	monto_final_pagado = tarjeta.realizar_pago(boleto)
  REPOSITORIO.agregar_tarjeta(tarjeta)

  halt 200, json({"cobrado":monto_final_pagado.to_i, "saldo":tarjeta.get_saldo.to_i,"id":id})
end

get '/tarjetas/:id' do
	id = params[:id]
	tarjeta = REPOSITORIO.buscar_tarjeta(id)

	halt 200, json({"id":tarjeta.get_id, "saldo":tarjeta.get_saldo.to_i, "modificadores":tarjeta.get_tipo_modificadores_precio_boleto.join(',')})
end

get '/tarjetas' do
	tarjetas = REPOSITORIO.get_tarjetas

	halt 200, json({"id":tarjeta.get_id, "saldo":tarjeta.get_saldo, "modificadores":tarjeta.get_tipo_modificadores_precio_boleto.join(',')})
end